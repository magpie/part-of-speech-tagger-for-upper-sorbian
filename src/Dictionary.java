/*
 *  Copyright (C) 2013 Daniil Sorokin daniil.sorokin@uni-tuebingen.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import baseclasses.Tuple;
import filefilters.ExtensionFilter;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;
import org.annolab.tt4j.TokenHandler;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



/**
 *
 * @author Daniil Sorokin <daniil.sorokin@student.uni-tuebingen.de>
 */
public class Dictionary {
    public static final String BASE_URL = "http://www.boehmak.de/cgi-bin/upreloz.cgi?pytaj=";
    public static final String BASE_URL_2 = "http://www.boehmak.de/cgi-bin/serbnem.cgi?pytaj=";
    public static final String ENCODING = "UTF-8";
    
    private static LinkedList<String> germanStack;
    private static HashSet<String> germanUsed;
    private static ArrayList<Tuple<String,String>> sorbianList;
    private static HashSet<Tuple<String,String>> sorbianListUnique;
    
    private static String germanTag;
    //Mapping for the parts of speech
    private static HashMap<String,String> tagsMapping = new HashMap<String,String>() {{ 
        put("p", "VERB"); 
        put("p\\ip", "VERB"); 
        put("ip", "VERB");
        put("adj", "ADJ"); 
        put("adv", "ADV"); 
        put("adw", "ADV");
        put("m", "NOUN"); 
        put("f", "NOUN"); 
        put("n", "NOUN"); 
        put("pers", "NOUN");
        put("konj", "CONJ");
        put("num", "NUM");
        put("prep", "PREP");
        put("praep", "PREP");
        put("indekl", "X");
        put("enkl", "ADP"); //enclitic
        put("det", "DET");
        put("subst", "NOUN");
        put("pron", "PRON");
        put("plt", "NOUN-plural");
    }};
    
    private static HashMap<String,String> sttsMapping = new HashMap<String,String>() {{ 
        put("ADJA", "ADJ-attrib"); 
        put("ADJD", "ADJ-pred"); 
        put("ADV", "ADV"); 
        put("APPR", "PREP"); 
        put("APPR", "PREP"); 
        put("APPRART", "PREP");
        put("APZR", "ADP");
        put("CARD", "NUM-nonpers-card");
        put("ITJ", "CONJ");
        put("KOUI", "CONJ");
        put("KOUS", "CONJ");
        put("KON", "CONJ");
        put("KOKOM", "PRT");
        put("NN", "NOUN");
        put("PDS", "PRON-demonstr");
        put("PDAT", "PRON-demonstr");
        put("PIS", "PRON-indef");
        put("PIAT", "PRON-indef");
        put("PIDAT", "PRON-indef");
        put("PPER", "PRON-reflexive");
        put("PPOSS", "PRON-posses");
        put("PPOSAT", "PRON-posses");
        put("PRELS", "PRON");
        put("PRELAT", "PRON");
        put("PRF", "PRON-reflexive");
        put("PWS", "PRON-interrog");
        put("PWAT", "PRON-interrog");
        put("PWAV", "PRON-interrog");
        put("PAV", "PRON-adv");
        put("PTKNEG", "PRT-neg");
        put("VVFIN", "VERB");
        put("VVIMP", "VERB");
        put("VVINF", "VERB");
        put("VVIZU", "VERB");
        put("VVPP", "VERB");
        put("VAFIN", "VERB");
        put("VAIMP", "VERB");
        put("VAINF", "VERB");
        put("VAPP", "VERB");
        put("VMFIN", "VERB");
        put("VMINF", "VERB");
        put("VMPP", "VERB");
    }};    
    /*
     * VERB - verbs (all tenses and modes)
     * NOUN - nouns (common and proper)
     * PRON - pronouns 
     * ADJ - adjectives
     * ADV - adverbs
     * ADP - adpositions (prepositions and postpositions)
     * CONJ - conjunctions
     * DET - determiners
     * NUM - cardinal numbers
     * PRT - particles or other function words
     * X - other: foreign words, typos, abbreviations
     * . - punctuation
     */
    
    
    public static void main(String[] args) throws MalformedURLException, UnsupportedEncodingException, IOException, BadLocationException {
        sorbianListUnique = new HashSet<Tuple<String, String>>();        
    }
    
    private static void readListOfGermanWords(String filename) throws IOException{
        BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream(filename), ENCODING));
        String line;
        while ((line = src.readLine()) != null){
            if (!line.startsWith("#")){
                line.replaceAll("\\(.*?\\)", "");
                line.replaceAll("[\\.,\\?\\!\\'\"]", "");
                germanStack.addAll(Arrays.asList(line.split(";")));
            }
        }
        src.close();
    }
    
    private static void collectSorbianWords() throws IOException, BadLocationException{
        sorbianList = new ArrayList<Tuple<String, String>>();
        while (germanStack.size() > 0){
            String queryString = germanStack.pop();
            queryString = queryString.replaceAll("\\(.*?\\)", "");
            queryString = queryString.replaceAll("[\\.,;:\\(\\)\\!\\?]", "");
            queryString = queryString.trim();
            
            if (!queryString.contains(" ") && queryString.matches("[A-Za-züöäßÜÖÄ]{2,}")){
                System.out.println(queryString);
                System.out.println(sorbianListUnique.size());
                InputStream input = new URL(BASE_URL + URLEncoder.encode(queryString, ENCODING)).openStream();
                StringWriter strwr = new StringWriter();
                IOUtils.copy(input, strwr, ENCODING);
                String responseString = strwr.toString();
                input.close();
                String[] parts;
                responseString = responseString.replaceAll("\\n", " ");
                if (responseString.contains("<TR>")){
                    responseString = responseString.replaceAll("<HTML>.*?<TR.*?>(.*?)</TR></TABLE>.*?</HTML>", "$1");
                    parts = responseString.split("</TR><TR>");
                } else {
                    parts = new String[0];
                }
                for (String part : parts){
                    String germPart = part.replaceAll("<TD>(.*?)</TD><TD>.*?</TD>", "$1");
                    String[] words = germPart.split("[,;~ ]");
                    for (String word : words){
                        word = word.replaceAll("<i>.*?</i>", "");
                        word = word.replaceAll("<.*?>", "");
                        word = word.replaceAll("\\(.*?\\)", "");
                        word = word.replaceAll("\\[.*?\\]", "");
                        word = word.replaceAll("[\\.,:;\\[\\]\\(\\)\\\\\\/]", "");
                        word = word.trim();
                        if (!germanUsed.contains(word)) {
                            germanStack.add(word);
                            germanUsed.add(word);
                        }
                    }
                    
                    String sorbPart = part.replaceAll("<TD>.*?</TD><TD>(.*?)</TD>", "$1");
                    sorbPart = sorbPart.replaceAll("(?<!\\<)/", ",");
                    words = sorbPart.split("[,;~]");
                    for (String word : words){
                        if (word.contains("<i>")){
                            String tag = word.replaceAll(".*?<i>(.*?)</i>.*", "$1");
                            word = word.replaceAll("<i>.*?</i>", "");
                            word = word.replaceAll("<.*?>", "");
                            word = word.replaceAll("\\(.*?\\)", "");
                            word = word.replaceAll("\\[.*?\\]", "");
                            word = word.replaceAll("[\\.,:;\\[\\]\\(\\)\\\\\\/]", "");
                            word = word.trim();
                            String extrTag = tag.trim().toLowerCase().replaceAll("[\\.,:;\\[\\]\\(\\)]", "");
                            if (tagsMapping.containsKey(extrTag))
                                sorbianListUnique.add(new Tuple<String, String>(tagsMapping.get(extrTag), word));
                            if (sorbianListUnique.size() % 300 == 0){
                                PrintWriter dest = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("SorbianPOSs.txt"), ENCODING)));
                                for (Tuple<String, String> tuple : sorbianListUnique) {
                                    dest.write(tuple.first + "\t" + tuple.second + "\n");
                                }
                                dest.close();
                            }
                        }
                    }
                }

            }
        }
    }
    
    private static void collectSorbianWords2() throws IOException{
        char[] alph = {'a', 'b', 'c', 'č', 'ć', 'd', 'ź', 
            'e', 'ě', 'f', 'g', 'h', 'i', 'j', 'k', 'ł' , 
            'l', 'm', 'n', 'ń', 'o', 'ó', 'p', 'q', 'r', 
            'ř', 's','š', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ž'};
        char[] alphInitial = {'a', 'b', 'c', 'č', 'ć', 'd', 
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'ł' , 
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 
            's', 'š', 't', 'u', 'v', 'w'};
        LinkedList<String> stack = new LinkedList<String>();
        for (char letter : alphInitial){
            stack.add("" + letter);
        }
        System.out.println("URls: " + stack.size());
        System.out.print("Get TreeTagger ... ");
        System.setProperty("treetagger.home", "C:/Program Files/TreeTagger");
        TreeTaggerWrapper<String> tt = new TreeTaggerWrapper<String>();
        //Allocate the model for the language in question
        tt.setModel("C:/Program Files/TreeTagger/lib/german.par:iso8859-1");
        //Set a handler to get a lemma and the POStag from the Tree Tagger
        tt.setHandler(new TokenHandler<String>() {
            public void token(String token, String pos, String lemma) {
                germanTag = pos;
            }
        });
        System.out.println("OK");
        /* Read links */
        while (stack.size() > 0){
            String queryString = stack.pop();
            queryString = queryString.replaceAll("\\(.*?\\)", "");
            queryString = queryString.replaceAll("[\\.,;:\\(\\)\\!\\?]", "");
            queryString = queryString.trim();
            System.out.println(queryString);
            
            System.out.println(sorbianListUnique.size());
            InputStream input = new URL(BASE_URL_2 + URLEncoder.encode(queryString, ENCODING)).openStream();
            StringWriter strwr = new StringWriter();
            IOUtils.copy(input, strwr, ENCODING);
            String responseString = strwr.toString();
            input.close();
            
            String word = "";
            for (String line : responseString.split("<TR>")){
                //System.out.println(line);
                if (line.startsWith("<TD>")){
                    line = line.replaceAll("<.*?>", "");
                    line = line.replaceAll("~.", "");
                    line = line.replaceAll("[\\.:;\\[\\]\\(\\)]", "");
                    String[] parts = line.split("[,\\s]");
                    word = parts[0];
                    String fTag = null;
                    for (int i = 1; i < parts.length; i++) {
                        String part = parts[i];
                        if (tagsMapping.containsKey(part)){
                            fTag = tagsMapping.get(part);
                            sorbianListUnique.add(new Tuple<String, String>(fTag, word));
                        }
                    }
                    for (int i = 1; fTag == null && i < parts.length; i++) {
                        String part = parts[i];
                        if (part.endsWith("-")){
                            fTag = tagsMapping.get("adj");
                            sorbianListUnique.add(new Tuple<String, String>(fTag, word));
                        } 
                    }
                    for (int i = 1; fTag == null && i < parts.length; i++) {
                        String part = parts[i];
                        try {
                            germanTag = null;
                            if (!part.trim().isEmpty()) 
                                tt.process(new String[] {part});
                            if (germanTag != null && sttsMapping.containsKey(germanTag)) {
                                fTag = sttsMapping.get(germanTag);
                                sorbianListUnique.add(new Tuple<String, String>(fTag, word));
                            }
                        } catch (TreeTaggerException ex) {
                            Logger.getLogger(Dictionary.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    /* Write to a file each 300 words. */
                    if (sorbianListUnique.size() % 300 == 0){
                        PrintWriter dest = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("SorbianPOSs_NEW.txt"), ENCODING)));
                        for (Tuple<String, String> tuple : sorbianListUnique) {
                            dest.write(tuple.first + "\t" + tuple.second + "\n");
                        }
                        dest.close();
                    }
                }
            }
            if (!word.isEmpty() && queryString.length() == 4 && word.length() > queryString.length()) {
                stack.offerFirst(word.substring(0,queryString.length()+1));
            }
            if (!word.isEmpty() && queryString.length() < 4)
                for (char letter : alph){
                    stack.offerFirst(queryString + letter);
                }
        }
        tt.destroy();
    }
    
    private static void analyzeOfflineDic(){
        File inputFile = new File("C:/Users/Даня/Documents/NLP Resources/Project/Dict/");
        File[] listOfFiles = inputFile.listFiles(new ExtensionFilter("html", false));
        if(listOfFiles != null) {
            for (File file : listOfFiles){
                try {
                    BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream(file), "iso-8859-2"));
                    for (String line; (line = src.readLine()) != null;){
                        if (line.startsWith("<p>")){
                            String word = line.replaceAll(".*?<b>(.*?)</b>.*", "$1");
                            word = word.replaceAll("<.*?>", "");
                            word = word.replaceAll("\\(.*?\\)", "");
                            word = word.replaceAll("\\[.*?\\]", "");
                            word = word.replaceAll("[\\.,:;\\[\\]\\(\\)\\\\\\/]", "");
                            word = word.trim();
                            String tag = line.replaceAll(".*?<i>(.*?)</i>.*", "$1");
                            if (tagsMapping.containsKey(tag)) {
                                tag = tagsMapping.get(tag);
                                System.out.println(word + "\t" + tag);
                                sorbianListUnique.add(new Tuple<String, String>(tag, word));
                            }
                        }
                    }
                    src.close();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Dictionary.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Dictionary.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        } 
    }
    
    private static void getTextsFromLeipzig() throws IOException{
        BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream("LeipzigCorporaLinks.txt"), ENCODING));
        ArrayList<String> links = new ArrayList<String>();
        String line;
        while ((line = src.readLine()) != null){
            links.add(line.trim());
        }
        src.close();
        PrintWriter dest = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("LeipzigTexts.txt"), ENCODING)));
        for (String link : links) {
            System.out.println(link);
            for (int i = 0; i < 30; i++) {
                InputStream input = new URL(link + URLEncoder.encode("&blocknr=" + i, ENCODING)).openStream();
                StringWriter strwr = new StringWriter();
                IOUtils.copy(input, strwr, ENCODING);
                String responseString = strwr.toString();
                input.close();
                Document doc = Jsoup.parse(responseString);
                Elements examplesList = doc.select("ul > li");
                System.out.println(examplesList.size());
                for (Element element : examplesList) {
                    dest.write(element.text().replaceAll("\\(.*?\\)", "") + "\n");
                }
            }
        }
        dest.close();
    }
}
