/*
 *  Copyright (C) 2013 Daniil Sorokin daniil.sorokin@uni-tuebingen.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import baseclasses.Tuple;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author Daniil Sorokin <daniil.sorokin@student.uni-tuebingen.de>
 */
public class TagObject {
    /* Parts of Speech rating */
    private static final ArrayList<String> POS_RATING = new ArrayList<String>(){{
        add("NOUN");
        add("VERB");
        add("ADP");
        add("ADJ");
        add("PRON");
        add("CONJ");
        add("PRT");
        add("ADV");
        add("NUM");
        add("INTERJ");
        add("ABBR");
        add(".");
        add("X");
    }};

    private ArrayList<Tuple<String,String>> tagPairs;

    /*
     * Default constructor.
     */
    public TagObject() {
        tagPairs = new ArrayList<Tuple<String, String>>();
    }
    
    /**
     * 
     * @param coreTag
     * @param attributes 
     */
    public void addTag(String coreTag, String attributes){
        Tuple<String,String> tag = new Tuple<String, String>(coreTag, attributes);
        if (!tagPairs.contains(tag)) tagPairs.add(tag);
    }
    
    /**
     * 
     */
    public void sort(){
        if (tagPairs.size() > 1) Collections.sort(tagPairs, new TagPairsComparator());
    }
    
    /**
     * 
     * @return 
     */
    public ArrayList<Tuple<String,String>> getAllTags(){
        return tagPairs;
    }
    
    /**
     * 
     * @return 
     */
    public ArrayList<String> getAllCoreTags(){
        ArrayList<String> coreTags = new ArrayList<String>();
        for (Tuple<String, String> tag : tagPairs) {
            coreTags.add(tag.first);
        }
        return coreTags;
    }
    
    /**
     * 
     * @return 
     */
    public String getTheMostProbableTag(){
        Tuple<String,String> tag = tagPairs.get(0);
        return tag.first + (tag.second.isEmpty() ? "" : "-" + tag.second);
    }
    
    /**
     * 
     * @return 
     */
    public String getTheMostProbableCoreTag(){
        return tagPairs.get(0).first;
    }
    
    /**
     * 
     * @return 
     */
    public boolean isEmpty(){
        return tagPairs.isEmpty();
    }
    
    /**
     * 
     * @return 
     */
    public String toString(){
        String rvalue = "";
        for (Tuple<String, String> tag : tagPairs) {
            rvalue = (!rvalue.isEmpty() ? rvalue + ", " : "") + tag.first + (tag.second.isEmpty() ? "" : "-" + tag.second);
        }
        return rvalue;
    }
    
    
    /**
     * 
     */
    private class TagPairsComparator implements Comparator{
        
        /**
         * 
         * @param o1
         * @param o2
         * @return 
         */
        public int compare(Object o1, Object o2) {
            String coreTag1 = ((Tuple<String,String>)o1).first;
            String coreTag2 = ((Tuple<String,String>)o2).first;            
            if (POS_RATING.indexOf(coreTag1) > POS_RATING.indexOf(coreTag2))
                return 1;
            else if (POS_RATING.indexOf(coreTag1) < POS_RATING.indexOf(coreTag2))
                return -1;
            else
                return 0;   
        }
    }
    
    /**
     * 
     * @param tag
     * @return 
     */
    public static String getCoreTag(String tag){
        return tag.split("[-]")[0];
    }
    
    /**
     * 
     * @param tag
     * @return 
     */
    public static String getAttributes(String tag){
        String[] tagParts = tag.split("[-]", 2);
        return tagParts.length > 1 ? tagParts[1] : "";
    }

}
