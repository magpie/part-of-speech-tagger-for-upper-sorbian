/*
 *  Copyright (C) 2013 Daniil Sorokin daniil.sorokin@uni-tuebingen.de
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import baseclasses.Triple;
import baseclasses.Tuple;
import com.google.common.collect.HashBasedTable;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import filefilters.ExtensionFilter;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.ricecode.similarity.JaroWinklerStrategy;
import org.apache.commons.cli.*;

/**
 *
 * @author Daniil Sorokin <daniil.sorokin@student.uni-tuebingen.de>
 */
public class POSTagger {
    
    /* Defining constants */
    public static final String ENCODING = "UTF-8";
    public static final double MIN_DISTANCE_FOR_FORMS = 0.9;
    public static final double PREFIX_SCALE_FOR_FORMS = 0.25;
    /* Dscribing constants */
    public static final String HEADER = "java -jar TaggerUpperSorbian.jar [OPTION]... [inputFileName/directoryName] [outputFile/directoryName]"
            + "\noptions:";

    /* Initial variables used for data loading */
    private static String dictionaryFileName;
    private static String morphologyFile;
    private static String closedClassListFileName;
    
    /* Statistics */
    private static ArrayList<Tuple<String,String>> results = new ArrayList<Tuple<String, String>>();
    private static double tagged = 0;
    
    private static HashMap<String, String> dictionary;
    private static HashMap<String, ArrayList<String>> closedClassWords;
    private static HashBasedTable<Character, String, String> morphology;
    private static boolean allPossibleTags;
    
    
    private static ArrayList<Triple> nounForms;
    private static ArrayList<Triple> verbForms;
    private static ArrayList<Triple> adjForms;
    private static ArrayList<Triple> advForms;
    
    private static Tuple[] specialRules = {
        //Proper names
        new Tuple<String,String>("\\p{javaUpperCase}\\p{javaLowerCase}*?","NOUN-proper"),
        //Punctuation
        new Tuple<String,String>("\\p{Punct}","."),
        //Numbers
        new Tuple<String,String>("[\\p{Digit}\\p{Punct}]+", "X"),
        //Abbreviations
        new Tuple<String,String>("[\\p{Upper}\\p{Digit}-]+", "ABBR")
    };
    
    private static String vowels = "aeěioóuy";
    
    private static HashMap<String,String> jCombinations = new HashMap<String,String>() {{ 
        put("sj", "š");
        put("cj", "č");
        put("zj", "ž");
        put("nj", "ń");
        put("kj", "c");
        put("hj", "z");
        put("chj", "š");
        put("tj", "ć");
        put("dj", "dź");
        put("ky", "ki");
        put("hy", "hi");
    }};
        
    public static void main(String[] args) {
        String input = null;
        String output = null; 
        String compareWith = null;
        boolean directories = false;
        boolean printStat = true;
        allPossibleTags = false;
        
        /* Default values */
        dictionaryFileName = "dictionary.txt";
        closedClassListFileName = "closedClassWords.txt";
        morphologyFile = "morphology.txt";
        
        /* Process commandline arugements */
        Options options = new Options();
        //Boolean options
        options.addOption("?", "help", false, "Prints this message");
        options.addOption("d", "directory", false, "Input and ouput are directories.  "
                + "In this case all compatible files in the input directory will be converted "
                + "and saved to the output directory. Subdirectories are not included!");
        options.addOption("stat", "statistics", false, "Prints out the statistics at the end of parsing.");
        options.addOption("a", "allTags", false, "The Tagger assigns all possible tags to a token.");
        //Argument options
        options.addOption("dict", "dictionary", true, "Specify dictionary file. Default value: dictionary.txt");
        options.addOption("ccList", "closedClassList", true, "Specify list of closed-class words. Default value: closedClassWords.txt");
        options.addOption("morph", "morphology", true, "Specify file with morphology. Default value: morphology.txt");
        options.addOption("comp", "compare", true, "Compares the results to a standard. "
                + "You have to specify the file with gold standard after the option. "
                + "Can only be used with files!");
        //A formatter is used to print out a help message.
        HelpFormatter formatter = new HelpFormatter();
        try {
            CommandLineParser parser = new GnuParser();
            // Parse the command line arguments
            CommandLine line = parser.parse(options, args);
            // Generate a help message if "help" option is used 
            // or there is some parameter missing.
            if(line.hasOption("?")) {
                formatter.printHelp(" ", HEADER, options, "");
                System.exit(0);
            } else if (line.getArgs().length > 2){
                Logger.getLogger(Converter.class.getName()).log(Level.WARNING, "You can only use two parameters.");
                formatter.printHelp(" ", HEADER, options, "");
                System.exit(0);
            } else {
                //Store parameters if everything is fine.
                directories = line.hasOption("d");
                allPossibleTags = line.hasOption("a");
                printStat = line.hasOption("stat");
                if (line.hasOption("dict"))
                    dictionaryFileName = line.getOptionValue("dict");
                if (line.hasOption("ccList"))
                    closedClassListFileName = line.getOptionValue("ccList");
                if (line.hasOption("morph"))
                    morphologyFile = line.getOptionValue("morph");
                if (line.hasOption("comp")){
                    if (!directories) compareWith = line.getOptionValue("comp");
                    else {
                        Logger.getLogger(Converter.class.getName()).log(Level.WARNING, "You can't use -comp option with directories.");
                        formatter.printHelp(" ", HEADER, options, "");
                        System.exit(0);
                    }
                }
                if (line.getArgs().length == 2) {
                    input = line.getArgs()[0];
                    output = line.getArgs()[1];
                } else if (line.getArgs().length == 1) {
                    input = line.getArgs()[0];                    
                }
            }
        } catch( ParseException ex) {
            Logger.getLogger(Converter.class.getName()).log(Level.SEVERE, null, "Arguments parsing failed.  Reason: " + ex.getMessage());
            formatter.printHelp(" ", HEADER, options, "");
            System.exit(0);
        }
        
        try {
            loadDictionary(dictionaryFileName);
            loadClosedClassWords(closedClassListFileName);
            loadMorphology(morphologyFile);
        } catch (IOException ex) {
            Logger.getLogger(POSTagger.class.getName()).log(Level.SEVERE, "Loading of one of th resources has failed\n {0}", ex.toString());
        }
        

        
        //If we deal with directories
        if (input != null && directories){
            File inputFile = new File(input);
            //Get all .txt files without subdirectories
            File[] listOfFiles = inputFile.listFiles(new ExtensionFilter("txt", false));
            if(listOfFiles != null) {
                if (output == null)
                    output = input;
                File outputFile = new File(output);
                if (outputFile.exists() && !outputFile.isDirectory()){
                    Logger.getLogger(Converter.class.getName()).log(Level.WARNING,
                            "-d option is used, but the output path is not a directory.");
                    System.exit(0);
                }
                if (!outputFile.exists())
                    outputFile.mkdirs();
                for (File file : listOfFiles){
                    String name = file.getName().replace(".txt", "_annotated.txt");
                    tagDataFromFile(file, new File (output + "/" + name));
                }
            } else {
                Logger.getLogger(Converter.class.getName()).log(Level.WARNING,
                        "-d option is used, but the input path is not a directory.\n"
                        + "or there is no compatible files.");
            }
        //If we deal with files
        } else if (input != null && !directories) {
            File inputFile = new File(input);
            if (inputFile.isFile() && ExtensionFilter.getExtension(inputFile).equals("txt")) {
                try {
                    if (output == null) tagDataFromStream(new FileInputStream(inputFile));
                    else tagDataFromFile(inputFile, new File(output));
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(POSTagger.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                Logger.getLogger(Converter.class.getName()).log(Level.WARNING, "The input file is not a file or is incompatible.");
            }
        } else {
            System.out.println("Enter a string:");
            tagDataFromStream (System.in);
        }
                
        if (printStat){
            double taggedRatio = tagged/results.size();
            System.out.println("Tagged: " + taggedRatio);
        }
        if (compareWith != null){
            try{
                System.out.println("Open evaluation set.");
                BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream(compareWith), ENCODING));
                ArrayList<Tuple<String,String>> evaluationSet = new ArrayList<Tuple<String, String>>();
                for(String line; (line = src.readLine()) != null;){
                    String[] parts = line.split("\\t", 2);
                    String coreTag = parts[1].split("-")[0];
                    evaluationSet.add(new Tuple<String, String>(parts[0], coreTag));
                }
                src.close();
                double tp = 0;
                double fp = 0;
                if (results.size() ==  evaluationSet.size()){
                    for (int i = 0; i < results.size(); i++) {
                        Tuple<String, String> observedPair = results.get(i);
                        Tuple<String, String> expPair = evaluationSet.get(i);
                        if (observedPair.first.equals(expPair.first)){
                            if (observedPair.second != null){
                                if (observedPair.second.equalsIgnoreCase(expPair.second))
                                    tp++;
                                else
                                    fp++;
                            }
                        } else {
                            System.out.println("Something is wrong with the evaluation set.\n"
                                    + observedPair.first + "\t" + expPair.first );
                        }
                    }
                    double pprecision = tp/(tp + fp);
                    System.out.println("Proper Precision: " + pprecision);
                    double precall = (tp)/results.size();
                    System.out.println("Proper Recall: " + precall);
                    double precision = tp/results.size();
                    System.out.println("Precision (Overall): " + precision);
                    double recall = (tp + fp)/results.size();
                    System.out.println("Recall (tagged): " + recall);
                } else {
                    System.out.println("Wrong file to compare with.");
                }
            } catch (IOException ex){
                Logger.getLogger(POSTagger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Tags data from the input stream and writes it to the outputFile
     * 
     * @param inputStream
     * @param outputFile 
     */
    public static void tagDataFromFile(File inputFile, File outputFile) {
        try {
            PrintWriter dest = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), ENCODING)));
            PTBTokenizer ptbt = new PTBTokenizer(new InputStreamReader(new FileInputStream(inputFile), ENCODING),
                    new CoreLabelTokenFactory(), "americanize=false,normalizeParentheses=false,"
                    + "normalizeOtherBrackets=false,asciiQuotes=true,invertible=true,untokenizable=allKeep");
            CoreLabel token;
            while (ptbt.hasNext()){
                token = (CoreLabel) ptbt.next();
                TagObject tag = getPOSTag(token.word());
                if (tag == null)
                    tag = getPOSTag(token.word().replaceFirst("(do|na|nad|nje|po|pod|pře|při|před|roz|wo|wob|wot|wu|za|naj)", ""));
//                dest.write(token.word() + "\t" + (tag != null ? tag : "No tag found") + "\n");
                dest.write(token.word() + "\t" + (allPossibleTags ?  tag : tag.getTheMostProbableTag()) + "\n");
                results.add(new Tuple<String, String>(token.word(), (tag != null ? tag.getTheMostProbableCoreTag() : null)));
                if (tag != null) tagged++;
            }
            dest.close();
        } catch (IOException ex) {
            Logger.getLogger(POSTagger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Tags data from an input stream.
     * 
     * @param inputStream 
     */
    public static void tagDataFromStream(InputStream inputStream) {
        try {
            PTBTokenizer ptbt = new PTBTokenizer(new InputStreamReader(inputStream, ENCODING),
                    new CoreLabelTokenFactory(), "americanize=false,normalizeParentheses=false,"
                    + "normalizeOtherBrackets=false,asciiQuotes=true,invertible=true,untokenizable=allKeep");
            CoreLabel token;
            while (ptbt.hasNext()){
                token = (CoreLabel) ptbt.next();
                TagObject tag = getPOSTag(token.word());
                if (tag == null)
                    tag = getPOSTag(token.word().replaceFirst("(do|na|nad|nje|po|pod|pře|při|před|roz|wo|wob|wot|wu|za|naj)", ""));
                //System.out.println(token.word() + "\t" + (tag != null ?  tag.getTheMostProbableCoreTag() : "No tag found"));
                System.out.println(token.word() + "\t" + (allPossibleTags ?  tag : tag.getTheMostProbableTag()));
                results.add(new Tuple<String, String>(token.word(), (tag != null ? tag.getTheMostProbableCoreTag() : null)));
                if (tag != null) tagged++;
            }
        } catch (IOException ex) {
            Logger.getLogger(POSTagger.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Returns a part of speech tag for a specific word.
     * 
     * @param word word
     * @return part of speech tag, null if there is no tag found
     */
    public static TagObject getPOSTag(String word){
        TagObject tagObject = new TagObject();
        String wordLower = word.toLowerCase();
        if (dictionary == null && closedClassWords == null && morphology == null){
            System.out.println("No information loaded!");
            return null;
        }
        /* Look up the word the word in the lists. */
        if (closedClassWords != null && closedClassWords.containsKey(wordLower)){
            for (String aTag : closedClassWords.get(wordLower)) {
                tagObject.addTag(TagObject.getCoreTag(aTag), TagObject.getAttributes(aTag));
            }
            tagObject.sort();
        }
        if (dictionary.containsKey(wordLower)){
            boolean wasEmpty = tagObject.isEmpty();
            tagObject.addTag(TagObject.getCoreTag(dictionary.get(wordLower)), 
                    TagObject.getAttributes(dictionary.get(wordLower)));
            if(wasEmpty) tagObject.sort();
        }
        if (morphology != null){
            boolean wasEmpty = tagObject.isEmpty();
            final char lastLetter = wordLower.charAt(wordLower.length()-1);
            if (morphology.contains(lastLetter, wordLower)){
                tagObject.addTag(TagObject.getCoreTag(morphology.get(lastLetter, wordLower)),
                        TagObject.getAttributes(morphology.get(lastLetter, wordLower)));
            } else if (tagObject.isEmpty() && morphology.containsRow(lastLetter)){
                final JaroWinklerStrategy jws = new JaroWinklerStrategy(PREFIX_SCALE_FOR_FORMS);
                Tuple<String, Double> currentCandidate = new Tuple<String, Double>(null, 0.0);
                for (String key : morphology.row(lastLetter).keySet()) {
                    double score = jws.score(wordLower, key);
                    if (score > MIN_DISTANCE_FOR_FORMS && score > currentCandidate.second){
                        currentCandidate = new Tuple<String, Double>(key, score);
                    }
                }
                if (currentCandidate.first != null) 
                    tagObject.addTag(TagObject.getCoreTag(morphology.get(lastLetter, currentCandidate.first)),
                            TagObject.getAttributes(morphology.get(lastLetter, currentCandidate.first)));
            }
            if(wasEmpty) tagObject.sort();
        }
        /* Check if the word belongs to a special category. */
        if (tagObject.isEmpty()){
            for (Tuple<String, String> rule : specialRules) {
                if (word.matches(rule.first)){
                    tagObject.addTag(TagObject.getCoreTag(rule.second), TagObject.getAttributes(rule.second));
                    break;
                }
            }
        }
        if (tagObject.isEmpty())
            tagObject.addTag("X", "");
        return tagObject;
    }
        
    /**
     * Read dictionary of words and parts of speech into a HashMap.
     * 
     * @param dicFile Name of the file
     * @throws IOException 
     */
    private static void loadDictionary(String dicFile) throws IOException{
        System.out.println("Loading initial tag distribution from " + dicFile + ". ");
        dictionary = new HashMap<String, String>();
        BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream(dicFile), ENCODING));
        String line;
        while ((line = src.readLine()) != null){
            if (!line.startsWith("#") && !line.contains(" ")){
                String[] parts = line.trim().split("\\t", 2);
                if (parts.length > 1) dictionary.put(parts[1].toLowerCase(), parts[0]);
            }
        }
        src.close();
//        System.out.println("Dictionaty is loaded. Size: " + dictionary.size());
    }
    
    /**
     * Loads morphology information from file.
     * 
     * @param morphFile
     * @throws IOException 
     */
    private static void loadMorphology(String morphFile) throws IOException {
        if (dictionary == null || dictionary.isEmpty()){
            System.out.println("No dictionary to generate forms.");
            return;
        }
        System.out.println("Loading morphology from " + morphFile + ". ");
        //generatedForms = new HashMap<String, String>();
        morphology = HashBasedTable.create();
        nounForms = new ArrayList<Triple>();
        verbForms = new ArrayList<Triple>();
        adjForms = new ArrayList<Triple>();
        advForms = new ArrayList<Triple>();
        BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream(morphFile), ENCODING));
        String line;
        while ((line = src.readLine()) != null){
            if (!line.startsWith("#")){
                line = line.replace("$", "");
                String[] parts = line.split("\\t", 3);
                if (parts.length > 2 && parts[2].startsWith("NOUN")) 
                    nounForms.add(new Triple<String,String,String>(parts[0], parts[1], parts[2]));
                if (parts.length > 2 && parts[2].startsWith("ADJ")) 
                    adjForms.add(new Triple<String,String,String>(parts[0], parts[1], parts[2]));
                if (parts.length > 2 && parts[2].startsWith("VERB")) 
                    verbForms.add(new Triple<String,String,String>(parts[0], parts[1], parts[2]));
                if (parts.length > 2 && parts[2].startsWith("ADV")) 
                    advForms.add(new Triple<String,String,String>(parts[0], parts[1], parts[2]));
            }
        }
        src.close();
        /* Generate all possible forms for basic parts of speech */
        System.out.println("Generating forms.");
        for (Entry<String,String> entry : dictionary.entrySet()){
            for (Tuple<String, String> tuple : generateForms(entry.getKey(), entry.getValue())) {
                //generatedForms.put(tuple.first, tuple.second);
                morphology.put(tuple.first.charAt(tuple.first.length()-1), tuple.first, tuple.second);
            }
        }
//        System.out.println("Morphology is loaded. Generated forms: " + morphology.size());
    }
    
    private static void loadClosedClassWords(String closedClassesFile) throws IOException {
        System.out.println("Loading closed classes from " + closedClassesFile + ". ");
        closedClassWords = new HashMap<String, ArrayList<String>>();
        BufferedReader src = new BufferedReader(new InputStreamReader(new FileInputStream(closedClassesFile), ENCODING));
        String line;
        while ((line = src.readLine()) != null){
            if (!line.startsWith("#")){
                String[] parts = line.trim().split("\\t", 2);
                if (parts.length > 1) {
                    String word = parts[0];
                    String tag = parts[1];
                    if (!closedClassWords.containsKey(word))
                        closedClassWords.put(word, new ArrayList<String>());
                    closedClassWords.get(word).add(tag);
                }
            }
        }
        src.close();
//        System.out.println("Closed-class words are loaded. Size: " + closedClasses.size());
    }
    
    private static boolean isVowel(char letter){
        String s = String.valueOf(letter).toLowerCase();
        return vowels.contains(s);
    }
    
    private static boolean isConsonant (char letter){
        return  !isVowel(letter);
    }
    
    private static HashSet<Tuple<String,String>> generateForms(String word, String pos) {
        HashSet<Tuple<String,String>> forms = new HashSet<Tuple<String, String>>();
        String coreTag = pos.split("-")[0];
        if (coreTag.equals("NOUN")) {
            for (Triple<String, String, String> nounForm : nounForms) {
                if (nounForm.first.equals(" ") && isConsonant(word.charAt(word.length()-1))){
                    String newForm = word;
                    newForm = newForm + nounForm.second;
                    newForm = newForm.trim();
                    forms.add(new Tuple<String, String>(newForm, nounForm.third));
                    if (newForm.contains("j")){
                        newForm = replaceJCombinations(newForm);
                        forms.add(new Tuple<String, String>(newForm, nounForm.third));
                    }
                } else if (word.endsWith(nounForm.first)) {
                    String newForm = word;
                    newForm = newForm.substring(0, newForm.lastIndexOf(nounForm.first));
                    newForm = newForm + nounForm.second;
                    forms.add(new Tuple<String, String>(newForm, nounForm.third));
                    if (newForm.contains("j")){
                        newForm = replaceJCombinations(newForm);
                        forms.add(new Tuple<String, String>(newForm, nounForm.third));
                    }
                }
            }
        } else if (coreTag.equals("VERB")) {
            for (Triple<String, String, String> verbForm : verbForms) {
                if (word.endsWith(verbForm.first)) {
                    String newForm = word;
                    newForm = newForm.substring(0, newForm.lastIndexOf(verbForm.first));
                    newForm = newForm + verbForm.second;
                    forms.add(new Tuple<String, String>(newForm, verbForm.third));
                    if (newForm.contains("j")){
                        newForm = replaceJCombinations(newForm);
                        forms.add(new Tuple<String, String>(newForm, verbForm.third));
                    }
                }
            }
            for (Triple<String, String, String> advForm : advForms) {
                if (word.endsWith(advForm.first)) {
                    String newForm = word;
                    newForm = newForm.substring(0, newForm.lastIndexOf(advForm.first));
                    newForm = newForm + advForm.second;
                    forms.add(new Tuple<String, String>(newForm, advForm.third));
                }
            }
        } else if (coreTag.equals("ADJ")) {
            for (Triple<String, String, String> adjForm : adjForms) {
                if (word.endsWith(adjForm.first)) {
                    String newForm = word;
                    newForm = newForm.substring(0, newForm.lastIndexOf(adjForm.first));
                    newForm = newForm + adjForm.second;
                    forms.add(new Tuple<String, String>(newForm, adjForm.third));
                }
            }
        } else if (coreTag.equals("PRON")) {
            for (Triple<String, String, String> adjForm : adjForms) {
                if (word.endsWith(adjForm.first)) {
                    String newForm = word;
                    newForm = newForm.substring(0, newForm.lastIndexOf(adjForm.first));
                    newForm = newForm + adjForm.second;
                    forms.add(new Tuple<String, String>(newForm, adjForm.third));
                }
            }
        }
        return forms;
    }
    
    private static String replaceJCombinations(String word){
        String rValue = word;
        rValue = rValue.replace("sj", "š");
        rValue = rValue.replace("cj", "č");
        rValue = rValue.replace("zj", "ž");
        rValue = rValue.replace("nj", "ń");
        rValue = rValue.replace("kj", "c");
        rValue = rValue.replace("hj", "z");
        rValue = rValue.replace("chj", "š");
        rValue = rValue.replace("tj", "ć");
        rValue = rValue.replace("dj", "dź");
        rValue = rValue.replace("ky", "ki");
        rValue = rValue.replace("hy", "hi");
        return rValue;
    }
}
